const rules = require('./webpack.rules');
const path = require('path'); 
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
rules.push({
  test: /\.css$/,
  use: [ 
    MiniCssExtractPlugin.loader,
    // { loader: 'style-loader' }, 
    { loader: 'css-loader'}],
});
rules.push({
  test: /\.(svelte)$/,
  use: {
    loader: 'svelte-loader',
    options: {
      emitCss: true,
    },
    
  }
});

rules.push({
  test: /node_modules\/svelte\/.*\.mjs$/,
  resolve: {
    fullySpecified: false
  }
});
module.exports = {
  resolve: {
		alias: {
			svelte: path.dirname(require.resolve('svelte/package.json'))
		},
		extensions: ['.mjs', '.js', '.svelte'],
		mainFields: ['svelte', 'browser', 'module', 'main']
	},
  plugins: [
    new MiniCssExtractPlugin({
			filename: '[name].css'
		})
  ],
  // Put your normal webpack config below here
  module: {
    rules
  },
};
