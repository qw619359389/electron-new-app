import { contextBridge, ipcRenderer } from 'electron'
// const { contextBridge, ipcRenderer } = require('electron')

contextBridge.exposeInMainWorld('app', {
    async login(account) {
        const result = await ipcRenderer.invoke("app", account);
        alert(result)
    },
    register() {

    }
})