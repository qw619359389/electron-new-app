import { app, BrowserWindow, ipcMain, net } from 'electron';

import path from 'path';
// Handle creating/removing shortcuts on Windows when installing/uninstalling.
// if (import('electron-squirrel-startup')) {
//   // eslint-disable-line global-require
//   app.quit();
// }
let loginWindow;
const createWindow = () => {
  // Create the browser window.
  loginWindow = new BrowserWindow({
    width: 800,
    height: 300,
    webPreferences: {
      // preload: MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY,
      preload: LOGIN_WINDOW_PRELOAD_WEBPACK_ENTRY,

    },
    // show:false
    // frame: false, // 无边框窗口
    autoHideMenuBar: true, // 自动隐藏菜单
    // transparent: true, //支持背景色为#AARRGGBB格式的可透明
    // backgroundColor: "#FFFFFF00",//窗口背景色
    // trafficLightPosition: { //设置控制按钮在无边框窗口中的位置
    //   x: 700,
    //   y: 100
    // }
  });
  // and load the index.html of the app.
  // mainWindow.loadURL("https://www.bilibili.com");
  // mainWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY);
  loginWindow.loadURL(LOGIN_WINDOW_WEBPACK_ENTRY);
  // Open the DevTools.
  // loginWindow.webContents.openDevTools();

};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.



// 登陆
ipcMain.handle("app", (event, access) => {
  return new Promise((resolve, reject) => {
    const bulicUrl = "https://www.boyeit.com/productHT/action/class/classLogin.php";
    const request = net.request({
      method: "POST",
      url: bulicUrl,
    });
    // 设置请求头
    request.setHeader("Accept", "application/json, text/javascript, */*; q=0.01; charset=UTF-8");
    request.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
    request.write("&username=" + access.user + "&password=" + access.password);
    request.on('response', (response) => {
      response.on('data', (chunk) => {
        const result = JSON.parse(chunk.toString("utf8"));
        console.log(result);
        // console.log(JSON.parse(result) + "=== nameerror "+ (JSON.parse(result)==="nameerror"));
        if (result === 'nameerror') {
          resolve("用户不存在");
        } else if (result === 'passerror') {
          resolve("密码错误");
        } else {
          const mainWindow = new BrowserWindow({
            webPreferences: {
              preload: MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY
            },
            show: false,
            // 窗口全屏
            // fullscreen: true,
            // 也是全屏
            // kiosk :true,
          });
          mainWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY);
          // 窗口最大化
          mainWindow.maximize();
          loginWindow.on('hide', () => {
            mainWindow.show();
            // mainWindow.webContents.openDevTools();
            loginWindow.close();
          });
          loginWindow.hide();
        }
        console.log(`BODY: ${chunk}`)
      })
      response.on('error', (error) => {
        reject(JSON.stringify(error));
        console.log(`ERROR: ${JSON.stringify(error)}`)
      })
    });
    request.end();
  })
});


import config from "./app/configs/config.js";

import hxzx from "./app/platform/hxzx/index.js"
import platform from "./app/platform"
console.log(config);


// const { platforms, platformsBasePath } = config;
// const autoProcedure = {};
// for (const platfrom of platforms) {
//   const platformPath = `${platfrom.value}/index.mjs`;
//   console.log(platformsBasePath);
//   // autoProcedure[platfrom.value] = import(path.join( "./native_modules/platform", platformPath));
// }


// 开始任务
ipcMain.handle("start", async (event, account) => {
  console.log(account);
  let a ;
  switch (account.platform) {
    case "hxzx":
      hxzx(account);
      break;
   
    default:
      a = await platform(account);
      break;
  }
  console.log(a);
  return a ;
});

