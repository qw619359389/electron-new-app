import platforms from "./platform.config.json";

export default {
    platformsBasePath: "/app/platform",
    platforms
}