

export default class {
    constructor(page) {
        /**@type {import('playwright').Page} */
        this.page = page
    }
    async getUser() {
        const data = await this.page.request.post("https://zjy2.icve.com.cn/api/student/Studio/index");
        try {
            const user = await data.json();
            return user;

        } catch (error) {
            return;
        }
    }
    async login(option) {
        const data = await this.page.request.post("https://zjy2.icve.com.cn/api/common/login/login", {
            form: option
        });
        return await data.json();
    }

    // 获取验证码并返回识别后的数据
    async getVCodeImgToBase64() {
        return await reconnection(async () => {
            const page = this.page;
            const result = await page.request.get("https://zjy2.icve.com.cn/api/common/VerifyCode/index");
            const buffer = await result.body();
            const base64Data = buffer.toString("base64");
            return base64Data;
        })

    }

    /**
     *  获取课程列表
     * @returns {Promise <{courseName:string,courseOpenId:string,openClassId:string,process:number}[]>}
     */
    async getCourseList() {
        return await reconnection(async () => {
            const data = await this.page.request.post("https://zjy2.icve.com.cn/api/student/learning/getLearnningCourseList");
            return (await data.json()).courseList;
        })

    }
    /**
     * 获取章节列表
     * @param {{ courseOpenId:string,openClassId:string}} option 
     * @returns 
     */
    async getProcessList(option) {
        return await reconnection(async () => {
            const data = await this.page.request.post("https://zjy2.icve.com.cn/api/study/process/getProcessList", {
                form: option
            });
            return (await data.json()).progress.moduleList;;
        })

    }

    /**
     * 获取题目列表
     * @param {{courseOpenId:string,moduleId:string}} option 
     * @returns 
     */
    async getTopicList(option) {
        return await reconnection(async () => {
            const data = await this.page.request.post("https://zjy2.icve.com.cn/api/study/process/getTopicByModuleId", {
                form: option
            });
            return (await data.json()).topicList
        })

    }
    /**
     * 获取单元列表
     * @param {{courseOpenId:string,openClassId:string,topicId:string}} option 
     * @returns 
     */
    async getCellList(option) {
        return await reconnection(async () => {
            const data = await this.page.request.post("https://zjy2.icve.com.cn/api/study/process/getCellByTopicId", {
                form: option
            });

            return (await data.json()).cellList
        })

    }
    /**
     * 任务信息
     * @param {{courseOpenId:string,openClassId:string,cellId:string,flag:"s",moduleId:string}} option 
     * @returns 
     */
    async getViewDirectory(option) {
        return await reconnection(async () => {
            const data = await this.page.request.post("https://zjy2.icve.com.cn/api/common/Directory/viewDirectory", {
                form: option
            });
            return await data.json()
        })

    }
    /**
   * 任务信息
   * @param {{courseOpenId:string,openClassId:string,cellId:string,flag:"s",moduleId:string}} option 
   * @returns 
   */
    async getLoadCellResource(option) {
        return await reconnection(async () => {
            const data = await this.page.request.post("https://zjy2.icve.com.cn/api/common/Directory/loadCellResource", {
                form: option
            });
            return await data.json()
        })

    }

    // https://zjy2.icve.com.cn/api/common/Directory/isLimitLearningDoc
    async getIsLimitLearningDoc(option) {
        return await reconnection(async () => {
            const data = await this.page.request.post("https://zjy2.icve.com.cn/api/common/Directory/isLimitLearningDoc", {
                form: option
            });
            return await data.json();
        })
    }
    // 如果返回的信息为-100 则发送此请求
    // https://zjy2.icve.com.cn/api/common/Directory/changeStuStudyProcessCellData
    async changeStuStudyProcessCellData(option){
        return await reconnection(async () => {
            const data = await this.page.request.post("https://zjy2.icve.com.cn/api/common/Directory/changeStuStudyProcessCellData", {
                form: option
            });
            return await data.json();
        })
    }
    // https://zjy2.icve.com.cn/api/study/process/hasProcessModulePower
    async getHasProcessModulePower(option){
        return await reconnection(async () => {
            const data = await this.page.request.post("https://zjy2.icve.com.cn/api/study/process/hasProcessModulePower", {
                form: option
            });
            return await data.json();
        })
    }
    /**
     * 发送日志
     * @param {{courseOpenId:string,openClassId:string,cellId:string,cellLogId:string,picNum:string,studyNewlyTime:string,studyNewlyPicNum:string,token:string}} option 
     * @returns 
     */
    async sendProcessCellLog(option) {
        return await reconnection(async () => {
            const data = await this.page.request.post("https://zjy2.icve.com.cn/api/common/Directory/stuProcessCellLog", {
                form: option
            })
            return await data.json();
        })
    }
}
import { setTimeout } from 'timers/promises'
// 重新连接
function reconnection(waitCallback, count = 5) {
    return new Promise(async resolve => {
        try {
            const result = await waitCallback();
            resolve(result);
        } catch (error) {
            console.log(error);
            console.log("Reconnect after 3 seconds");
            await setTimeout(1000);
            console.log(1);
            await setTimeout(1000);
            console.log(2);
            await setTimeout(1000);
            console.log(3);
            console.log("Reconnect...");
            resolve(await reconnection(waitCallback, (count - 1)));
        }
    });
}

