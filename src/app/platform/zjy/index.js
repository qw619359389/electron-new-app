import request from './request';
import { setTimeout } from 'timers/promises'

export default class {

  constructor(page) {
    /**@type {import('playwright').Page} */
    this.page = page
    this.request = new request(page);
  }

  /**
   * @param {{user:string,password:string}} account 
   */
  async login(account) {
    /** {
            code: -16
            msg: "验证码输入错误！"
            need_verifycode: true
        },
        {
            code: -2
            msg: "用户密码错误！"
        }
    */
    // 检查是否已经登陆
    const user = await this.request.getUser();
    if (user) {
      console.log("already");
      return user;
    }
    const vCodeImgBase64 = await this.request.getVCodeImgToBase64();
    const code = await vCode(vCodeImgBase64);

    const verifyCode = code.data.result;

    const data = await this.request.login({
      userName: account.user,
      userPwd: account.password,
      verifyCode
    });
    console.log("not login");
    return data
  }


  async automaticProgram() {
    let inspection = 0;
    const request = this.request;
    return new Promise(async resolve => {

      // 每隔一分钟看一次啊程序是否卡主不动了，如果10分钟没有更新，则重启
      const t = setInterval(async () => {
        if (inspection < 10) {
          inspection++
        } else {
          clearInterval(t);
          resolve(await this.automaticProgram());
        }
      }, 60000);

      console.info("getCourseList : start");

      const courseList = await request.getCourseList();
      console.log("get course list success");
      await each(courseList, async (course) => {
        const { courseOpenId, openClassId } = course;

        if (course.process < 100) {
          console.info("getProcessList : start");

          const processList = await request.getProcessList({
            courseOpenId,
            openClassId
          });
          console.log("get process list success");
          await each(processList, async (process) => {
            if (process.percent < 100) {
              console.info("getTopicList : start");
              const topicList = await request.getTopicList({
                courseOpenId,
                moduleId: process.id
              });

              console.log("get topic list success");

              await each(topicList, async (topic) => {
                console.info("getCellList : start");

                const cellList = await request.getCellList({
                  courseOpenId,
                  openClassId,
                  topicId: topic.id
                });

                console.log("get cell list success");

                await each(cellList, async (cell) => {
                  const option = {
                    courseOpenId,
                    openClassId,
                    flag: "s",
                    moduleId: process.id,
                    upTopicId: topic.id,
                    upCellId: 0,
                    isFirstModule: 0
                  }
                  if (cell.childNodeList.length > 0) {
                    await each(cell.childNodeList, async (item) => {
                      let percent = item.stuCellFourPercent
                      if (percent === undefined) {
                        percent = item.stuCellPercent
                      }
                      if (percent === undefined) {
                        percent = item.cellPercent
                      }
                      console.log(item);
                      console.info("cell percent :" + percent, typeof percent, percent < 100);

                      if (percent < 100) {
                        console.warn("startSendLog");

                        await startSendLog({
                          ...option,
                          cellId: item.Id,
                          cellUrl: item.resourceUrl,
                        });
                      }
                    },200);
                  } else {
                    let percent = cell.stuCellFourPercent
                    if (percent === undefined) {
                      percent = cell.stuCellPercent
                    }
                    if (percent === undefined) {
                      percent = cell.cellPercent
                    }
                    console.log(cell);

                    console.info("cell percent :" + percent, typeof percent, percent < 100);

                    if (percent < 100) {
                      console.warn("startSendLog");
                      await startSendLog({
                        ...option,
                        cellId: cell.Id,
                        cellUrl: cell.resourceUrl
                      });
                    }
                  }
                })
              });
            }
          });

        }
      });
      resolve();
    })


    /**
     * 数组遍历方法，间隔1秒
     * @param {any[]} list 
     * @param {(any)=>Promise<any>} callback 
     */
    async function each(list, callback, time = 1000) {
      for (let i = 0; i < list.length; i++) {
        await setTimeout(time);
        await callback(list[i], i, list)
      }
    }
    // 开始发送日志
    function startSendLog(option) {
      return new Promise(async (resolve) => {
        // 是个限制请求？
        const limit = await request.getIsLimitLearningDoc({
          cellId: option.cellId
        });
        console.log(limit);
        // 一个不知道什么用途的请求， 但是原平台每次获取任务前都发送
        const power = await request.getHasProcessModulePower(option);
        console.log(power);

        console.info("viewDetail ViewDirectory: start");

        // 获取任务详情
        let viewDetail = await request.getViewDirectory(option);
        console.info(viewDetail);
        
        if (viewDetail.code !== 1) {
          if (viewDetail.code == -100) {
            // 详情请求失败，发送此请求
            const changeStuStudyProcessCellData = await request.changeStuStudyProcessCellData({
              ...option,
              cellName: viewDetail.currCellName
            });
            console.log(changeStuStudyProcessCellData);
          }
          console.info("viewDetail LoadCellResource: start");
          viewDetail = await request.getLoadCellResource(option);
        }
        console.info("get cell list : success");
        console.log(viewDetail);
        const {cellName, cellPercent, categoryName, audioVideoLong, stuStudyNewlyTime, stuStudyNewlyPicCount, cellLogId, guIdToken } = viewDetail
        console.log("cellPercent:" + cellPercent);
        delete option.flag
        delete option.moduleId
        let cellLogOption = {
          ...option,
          cellLogId,
          token: guIdToken,
          picNum: 0,
          studyNewlyTime: 0,
          studyNewlyPicNum: 0
        }
        let sendCellLog = async function (Option) {
          console.info("picture or swf cell log ");

          const result = await request.sendProcessCellLog(Option);
          console.log(result);
          if (result.code == 1) {
            return true;
          }
          return false;
        };
        if (categoryName.indexOf("图片") >= 0) {
          console.log('add a picture task');
          cellLogOption.picNum = 1;
          cellLogOption.studyNewlyPicNum = 1;
        } else if (categoryName.indexOf("文档") >= 0) {
          console.log('add a document task');

          let page = 0;
          sendCellLog = async function () {
            console.info("document cell log ");
            page++;
            if (page < stuStudyNewlyPicCount) {
              cellLogOption.picNum = page;
              cellLogOption.studyNewlyPicNum = page;
              const result = await request.sendProcessCellLog(cellLogOption);
              console.log(page, result);
              if (result.code !== 1) {
                page--;
              }
              return false;
            } else {
              page = null;
              cellLogOption = null;
              return true;
            }
          };
        } else if (categoryName.indexOf("音频") >= 0 || categoryName.indexOf("视频") >= 0) {

          console.log('add a video task');

          let currentViewTime = stuStudyNewlyTime;
          let duration = audioVideoLong
          if (currentViewTime >= duration) {
            currentViewTime = 0;
          }
          sendCellLog = async function () {
            console.info("video cell log ");

            let flag = false;
            const old = currentViewTime
            currentViewTime += 10 + Math.random();
            if (currentViewTime >= duration) {
              currentViewTime = duration
              flag = true;
            }
            cellLogOption.studyNewlyTime = currentViewTime.toFixed(6);
            const result = await request.sendProcessCellLog(cellLogOption);
            console.log(currentViewTime, result);

            if (result.code != 1) {
              currentViewTime = old;
              flag = false;
            }
            return flag
          }
        }
        console.count("timer");
        const timer = setInterval(async () => {
          inspection = 0;
          console.log(cellName,":",categoryName);
          const result = await sendCellLog(cellLogOption);
          if (result) {
            clearInterval(timer);
            resolve();
          }
        }, 10000);
      })
    }


  }
}





import { net } from 'electron'
//识别验证码
function vCode(base64Data) {
  return new Promise(
    /**
     * @param {(value: {success:Boolean,code: '0'|'1',message: String,data:{result:String,id:String}}) => void} resolve
     */
    (resolve, reject) => {
      const request = net.request({
        url: "http://api.ttshitu.com/predict",
        method: "POST",
      })
      const username = "qw619359389"
      const password = "qw1234567890";
      const typeid = "1001";

      request.setHeader("Accept", "application/json, text/javascript, */*; q=0.01; charset=UTF-8");
      request.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")

      request.write("&username=" + username + "&password=" + password + "&typeid=" + typeid + "&image=" + base64Data);
      request.on('response', (response) => {
        response.on("data", (chunk) => {

          resolve(JSON.parse(chunk.toString("utf-8")));
        });
        response.on("error", (error) => {
          reject("error:" + error)
        });
      });
      request.end();
    })
}