

import { chromium } from 'playwright';
import { access } from 'fs/promises'
import { constants } from 'fs';

import { screen } from 'electron';

import { setTimeout } from 'timers/promises';


export default function (account) {
    return new Promise(async resolve => {



        const browser = await chromium.launch({
            // 使用本机已经有的chrome浏览器，本机需要安装chrome，使用chrome的原因是chromium不能解码mp4视频
            channel: "chrome",
            headless: false,
        });


        // 从electron中的screen 方法中获取屏幕的宽高，这个方法只能在进程加载完成后调用
        const primaryDisplay = screen.getPrimaryDisplay();

        const pageConfig = {
            // 页面尺寸设置为屏幕宽高
            viewport: primaryDisplay.workAreaSize
        };




        const storageStatePath = `storageState/${account.platform + account.user}.json`;
        let isLogin = false
        try {
            // 查看是否有登陆后的文件,有则设置isLogin为TRUE,没有则抛出错误
            await access(storageStatePath, constants.F_OK);
            isLogin = true;
        } catch (error) { }

        if (isLogin) {
            // 登陆过，读取保存的账号cookie
            pageConfig.storageState = storageStatePath;
        }

        const browserContext = await browser.newContext(pageConfig);

        const page = await browserContext.newPage();


        let first = false
        await page.route(/login\.hexuezx\.cn\/$/g, async (route, request) => {
            await route.continue();
            await page.waitForNavigation();
            //  登陆页会加载两次，所以使用一个标记变量，只在第一次调用
            !first && (first = true, await login(page));
        })

        // 打开我的课程页面
        await page.goto("http://student.hexuezx.cn/#/my-course");


        // 如果不是在 我的课程页面，则等待
        if (!page.url().includes("student.hexuezx.cn/#/my-course")) {
            await page.waitForNavigation({
                url: /student\.hexuezx\.cn\/\#\/my\-course$/g,
                waitUntil: "domcontentloaded",
                timeout: 0
            });
        }

        // 方法 等待
        function Sleep(number) {
            return new Promise(resolve => {
                const timer = setTimeout(() => {
                    clearTimeout(timer);
                    resolve();
                    return;
                }, number);
            });
        }



        const listResponse = await page.waitForResponse('**/LearningSpace/list');



        // 登陆后保存账号信息（cookie和localStorage）
        page.context().storageState({ path: storageStatePath });
        const courseList = (await listResponse.json()).data.list;



        for (let i = 0; i < courseList.length; i++) {
            const course = courseList[i];
            if (parseFloat(course.progress) < 100) {

                const authToken = await page.evaluate(() => {
                    return localStorage.getItem("AUTH_TOKEN");
                });
                // headers 中添加身份验证
                const headers = {
                    "access-token": JSON.parse(authToken)
                }
                const courseDirectoryProcess = await page.request.get("http://api.zaixian.hexuezx.cn/studyLearn/courseDirectoryProcess?courseOpenId=" + course.id, {
                    headers
                });


                const { moduleList } = (await courseDirectoryProcess.json()).data;
                // console.log(moduleList);
                for (let j = 0; j < moduleList.length; j++) {
                    const chapter = moduleList[j];
                    if (chapter.percent < 100) {
                        const topics = chapter.topics
                        for (let k = 0; k < topics.length; k++) {
                            const topic = topics[k];
                            console.log("topic:", topic);
                            if (topic.percent < 100) {
                                const cells = topic.cells;
                                for (let c = 0; c < cells.length; c++) {
                                    const cell = cells[c];
                                    if (cell.type == 1 && cell.process < 100) {

                                        //  获取视频详情
                                        const cellDetail = await page.request.get("http://api.zaixian.hexuezx.cn/studyLearn/cellDetail?cellId=" + cell.id, {
                                            headers
                                        });
                                        // 视频详情
                                        const detail = await cellDetail.json();
                                        // 视频总时长
                                        const duration = cell.videoLength;
                                        // 计数，发了几次请求
                                        let count = 1;
                                        // 计时器循环发送log请求
                                        const timer = setInterval(async () => {

                                            let videoEndTime = detail.data.longestVideoEndTime + (60 * count);
                                            if (videoEndTime >= duration) {
                                                // 清空计数器
                                                count = null;
                                                // 如果观看时长大于或等于视频总时长，把观看时长设置为视频总时长，并结束计时器。观看完成
                                                videoEndTime = duration;
                                                clearInterval(timer);
                                                console.log("end");
                                            }
                                            const result = await page.request.post("http://api.zaixian.hexuezx.cn/studyLearn/leaveCellLog", {
                                                data: {
                                                    id: detail.data.cellLogId,
                                                    stopSeconds: 0,
                                                    videoEndTime
                                                },
                                                headers,
                                            });
                                            count && count++;
                                            console.log(await result.json());
                                        }, 60000);
                                        console.count("create:setInterval");
                                    }
                                }
                                // await Sleep(5000);
                                await setTimeout(5000);
                            }
                        }
                        // await Sleep(2000);
                        await setTimeout(2000);

                    }
                }
                // await Sleep(1000);
                await setTimeout(1000);

            }
        }



        /**
         * 这里是登陆
         * @param { import('playwright').Page } page 
         */
        async function login(page) {
            // 获取选择学校元素
            const schoolSelector = await page.locator("#pane-login > form > li:nth-child(1) > div.selectBox > div > div > div > div > input");
            // 点击元素
            await schoolSelector.click();
            // 输入内容
            await schoolSelector.fill("山东建筑");
            // 获取学校列表，并点击山东建筑学校
            await page.locator("body > div.el-select-dropdown.el-popper > div.el-scrollbar > div.el-select-dropdown__wrap.el-scrollbar__wrap > ul > li:nth-child(26) > span").click();
            // 获取 用户输入框
            const userInput = await page.locator('#pane-login > form > li:nth-child(3) > div.selectBox > div > div > div.el-input > input');
            // 点击输入框
            await userInput.click();
            // 填写账号
            await userInput.fill(account.user);
            // 获取密码输入框
            const passwordInput = await page.locator('[placeholder="请输入密码"]');
            // 点击输入框
            await passwordInput.click();
            // 填写密码
            await passwordInput.fill(account.password);
            // Click #pane-login >> text=登录
            // 点击登陆按钮
            await Promise.all([
                page.waitForNavigation(/*{ url: 'http://student.hexuezx.cn/#/home' }*/),
                page.locator('#pane-login >> text=登录').click()
            ]);

            // Click text=我的课程
            // 进入我的课程
            await page.locator('text=我的课程').click();
        }


        // 截图
        // page.screenshot({ path: 'screenshot/screenshot.png', fullPage: true });
        resolve();
        return true;

    });
}