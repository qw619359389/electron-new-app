

import { chromium } from 'playwright';
import { access } from 'fs/promises'
import { constants } from 'fs';
import { screen } from 'electron';
import zjy from './zjy';
import cx from './cx'

export default async function (account) {
    const browser = await chromium.launch({
        // 使用本机已经有的chrome浏览器，本机需要安装chrome，使用chrome的原因是chromium不能解码mp4视频
        // channel: "chrome",
        headless: false,
    });


    // 从electron中的screen 方法中获取屏幕的宽高，这个方法只能在进程加载完成后调用
    const primaryDisplay = screen.getPrimaryDisplay();

    const pageConfig = {
        // 页面尺寸设置为屏幕宽高
        viewport: primaryDisplay.workAreaSize
    };




    const storageStatePath = `storageState/${account.platform + account.user}.json`;
    let isLogin = false
    try {
        // 查看是否有登陆后的文件,有则设置isLogin为TRUE,没有则抛出错误
        await access(storageStatePath, constants.F_OK);
        isLogin = true;
    } catch (error) { }

    if (isLogin) {
        // 登陆过，读取保存的账号cookie
        pageConfig.storageState = storageStatePath;
    }

    const browserContext = await browser.newContext(pageConfig);

    const page = await browserContext.newPage();
    let platformProgram ;

    switch (account.platform) {
        case "zjy":
            platformProgram = new zjy(page)
            break;
        case "cx":
            platformProgram = new cx(page)
            break;
        default:
            break;
    }
    // 登陆
    const a = await platformProgram.login(account);

    // 登陆成功后，保存账号信息
    page.context().storageState({ path: storageStatePath });
    console.log("login: success");
    // 开始自动挂机
    platformProgram.automaticProgram();
    console.log("Started to hang up");
    return a;


}



